package labyrinth;

import java.awt.Color;

import datastructure.GridDirection;
import datastructure.IGrid;
import datastructure.Location;

public class Labyrinth implements ILabyrinth {
	private final IGrid<LabyrinthTile> tiles;
	// private int playerX = -1;
	// private int playerY = -1;
	private Location playerLoc;
	private Location monsterLoc;
	private LabyrinthTile prev = LabyrinthTile.OPEN;

	// for gold and hp count
	private int gold;
	private int hp = 100;
	private static int goldOnMap;
	private static int goldAtStart;
	private boolean monsterExist = false;

	boolean playerSet;

	public Labyrinth(IGrid<LabyrinthTile> tiles) throws LabyrinthParseException {
		if (tiles == null) {
			throw new IllegalArgumentException();
		}

		this.tiles = tiles;

		int numPlayers = 0;
		for (Location loc : tiles.locations()) {
			if (tiles.get(loc) == LabyrinthTile.GOLD) {
				goldOnMap++;
				goldAtStart++;
			}
			if (tiles.get(loc) == LabyrinthTile.PLAYER) {
				numPlayers++;
				playerLoc = loc;
				playerSet = true;
			}
			if (tiles.get(loc) == LabyrinthTile.MONSTER) { //sets monsterLoc at start, and is updated in monsterMove.
				monsterLoc = loc;						   //also sets monsterExist to true, to fix a bug in games without monsters.
				monsterExist = true;
			}
		}
		if (numPlayers != 1) {
			throw new LabyrinthParseException("Labyrinth created with " + numPlayers + " number of players!");
		}

		checkState(this);
	}

	public static void checkState(Labyrinth labyrinth) {
		boolean ok = !labyrinth.playerSet || labyrinth.isValidPos(labyrinth.playerLoc);
		int numPlayers = 0;
		int goldMap = 0;
		for (Location loc : labyrinth.tiles.locations()) {
			if (labyrinth.tiles.get(loc) == LabyrinthTile.PLAYER) {
				numPlayers++;
			}
			if (labyrinth.tiles.get(loc) == LabyrinthTile.GOLD) {
				goldMap++;
			}
		}
		goldOnMap = goldMap;

		if (labyrinth.playerSet) {
			ok &= numPlayers == 1;
		} else {
			ok &= numPlayers == 0;
		}
		
		if (labyrinth.getPlayerHitPoints() <= 0) {  // you die if your hp reaches 0 or less.
			labyrinth.playerSet = false;
		}
		if (goldAtStart != 0 && goldOnMap == 0) {   // both conditions necessary to not end the labyrinth game
			labyrinth.playerSet = false;            // from a loaded grid, and starts without any gold.
		}
		if (!ok) {
			throw new IllegalStateException("bad object");
		}
	}

	@Override
	public LabyrinthTile getCell(Location loc) {
		checkPosition(loc);

		return tiles.get(loc);
	}

	@Override
	public Color getColor(Location loc) {
		if (!isValidPos(loc)) {
			throw new IllegalArgumentException("Location invalid");
		}
		return tiles.get(loc).getColor();
	}

	@Override
	public int numberOfRows() {
		return tiles.numRows();
	}

	@Override
	public int getPlayerGold() {
		return gold;
	}

	public int getGoldOnMap() {
		return goldOnMap;
	}

	@Override
	public int getPlayerHitPoints() {
		return hp;
	}

	@Override
	public int numberOfColumns() {
		return tiles.numColumns();
	}

	@Override
	public boolean isPlaying() {
		return playerSet;
	}

	private boolean isValidPos(Location loc) {
		return loc.row >= 0 && loc.row < tiles.numRows() //
				&& loc.col >= 0 && loc.col < tiles.numColumns();
	}

	private void checkPosition(Location loc) {
		if (!isValidPos(loc)) {
			throw new IndexOutOfBoundsException("Row and column indices must be within bounds");
		}
	}

	@Override
	public void movePlayer(GridDirection d) throws MovePlayerException {
		
		if (!playerCanGo(d)) {           // check pre-conditions
			hp -= 5;                     // to avoid walking through walls or outside the grid.
			throw new MovePlayerException("This is not a legal move");
		}

		Location newLoc = playerLoc.getNeighbor(d);
		tiles.set(playerLoc, LabyrinthTile.OPEN);
		playerLoc = newLoc;

		// for gold and hp
		boolean goldFound = false;
		if (getCell(newLoc) == LabyrinthTile.GOLD) {
			goldFound = true;
		}
		tiles.set(newLoc, LabyrinthTile.PLAYER);

		if (monsterExist) {    // only calls monsterMove if there exists a monster on the grid
			monsterMove();     // in case of the grid without a monster.
		}
		checkState(this);

		if (goldFound) {       // when you find gold, you gain 10 hp.
			gold++;
			if (hp < 91) {
				hp += 10;
			} else {
				hp = 100;
			}
		}
	}

	@Override
	public boolean playerCanGo(GridDirection d) {
		if (d == null) {
			throw new IllegalArgumentException();
		}

		return playerCanGoTo(playerLoc.getNeighbor(d));
	}

	/**
	 * This method checks if a player can move to a given location A player can not
	 * go to the location if there is a wall or if the location is outside the
	 * bounds of the grid.
	 * 
	 * @param loc
	 * @return
	 */
	private boolean playerCanGoTo(Location loc) {
		if (!isValidPos(loc)) {
			return false;
		}

		return tiles.get(loc) != LabyrinthTile.WALL;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (int y = tiles.numColumns() - 1; y >= 0; y--) {
			for (int x = 0; x < tiles.numRows(); x++) {
				sb.append(getSymbol(new Location(x, y)));
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	/**
	 * No bounds checking will be done for the given {@code loc}.
	 */
	private String getSymbol(Location loc) {
		return String.valueOf(tiles.get(loc).getSymbol());
	}

	@Override
	public Iterable<Location> locations() {
		return tiles.locations();
	}

	/**
	 * Makes the monster move towards the player when called,
	 * and repaints the previous tile to what is was before the monster
	 * stepped on it.
	 * If the monster catches the player, the player loses 40 hp, and
	 * the monster is re-spawned somewhere else on the grid.
	 */
	public void monsterMove() {
		Location move = monsterNextLocation();
		tiles.set(monsterLoc, prev);					//resets the tile to what is previously was after the monster moves away.
		prev = getCell(move);
		if (prev == LabyrinthTile.PLAYER) { 			//spawns more player tiles without this check
			prev = LabyrinthTile.OPEN;
			hp -= 40;
		}
		if (getCell(move) == LabyrinthTile.PLAYER) {
			Location newL = LabyrinthHelper.monsterSpawn(playerLoc.col, playerLoc.row, tiles);
			tiles.set(newL, LabyrinthTile.MONSTER);			//re-spawns a new monster when the player has been hit
			monsterLoc = newL;
		} else {
			monsterLoc = move;
			tiles.set(move, LabyrinthTile.MONSTER);
		}
	}
/**
 * checks neighbors to get available moves and finds
 * the move that takes the monster the shortest distance 
 * from the player.
 * @return the next location for the monster to move to
 */
	public Location monsterNextLocation() {
		Location temp = null;
		Location[] options = getNeighbours(monsterLoc);
		int playerX = playerLoc.col;
		int playerY = playerLoc.row;
		int totalDiff = -1;
		for (Location l : options) {
			int newX = l.col;
			int newY = l.row;
			int xDiff = posDiff(playerX, newX);
			int yDiff = posDiff(playerY, newY);
			if (totalDiff > xDiff + yDiff) {
				totalDiff = xDiff + yDiff;
				temp = l;
			} else if (totalDiff == -1) {
				totalDiff = xDiff + yDiff;
				temp = l;
			}
		}
		return temp;
	}
	/**
	 * @param playerX
	 * @param newX
	 * @return returns the positive distance between the new move and the player in one dimension.
	 */
	public int posDiff(int playerX, int newX) {
		int differance = 0;
		if (playerX > newX) {
			while (playerX != newX) {
				newX++;
				differance++;
			}
		} else if (playerX < newX) {
			while (playerX != newX) {
				playerX++;
				differance++;
			}
		}
		return differance;
	}
	/**
	 * the monster can move in all GridDirections, including staying still.
	 * 
	 * @param loc
	 * @return a table of neighbor locations that the monster can move to. 
	 */
	public Location[] getNeighbours(Location loc) {
		GridDirection[] tab = GridDirection.values();
		Location[] retur = new Location[tab.length];
		int i = 0;
		for (GridDirection x : tab) {
			if (monsterCanGo(x)) {
				retur[i] = loc.getNeighbor(x);
				i++;
			}
		}
		Location[] trimmed = new Location[i];
		int k = 0;
		for (int j = 0; j < retur.length; j++) {
			if (retur[j] != null) {
				trimmed[k] = retur[j];
				k++;
			}
		}
		return trimmed;
	}
	/**
	 * 
	 * @param d
	 * @return boolean value of whether the monster can move in a grid direction.
	 */
	public boolean monsterCanGo(GridDirection d) {
		if (d == null) {
			throw new IllegalArgumentException();
		}
		return playerCanGoTo(monsterLoc.getNeighbor(d));
	}

}
