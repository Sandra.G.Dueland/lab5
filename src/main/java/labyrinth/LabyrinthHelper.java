package labyrinth;

import java.util.Random;

import datastructure.Grid;
import datastructure.IGrid;
import datastructure.Location;

public class LabyrinthHelper {

	private static boolean test = false;
	
	public static IGrid<LabyrinthTile> loadGrid(char[][] source) {
		int rows = source.length;
		int cols = source[0].length;
		test = true;

		IGrid<LabyrinthTile> grid = new Grid<LabyrinthTile>(rows, cols, LabyrinthTile.OPEN);
		for (int col = 0; col < cols; col++) {
			for (int row = 0; row < rows; row++) {
				Location loc = new Location(row, col);
				char symbol = source[row][col];
				LabyrinthTile tile = LabyrinthTile.fromSymbol(symbol);

				if (tile == null) {
					throw new LabyrinthParseException(
							"Incorrect symbol '" + symbol + "' in labyrinth at row " + row + ", col " + col);
				}
				grid.set(loc, tile);
			}
		}

		return grid;
	}

	public static IGrid<LabyrinthTile> makeRandomGrid(int width, int height) {
		Random random = new Random();

		IGrid<LabyrinthTile> grid = new Grid<LabyrinthTile>(width, height, LabyrinthTile.OPEN);
		for (Location loc : grid.locations()) {
			int r = random.nextInt(20);
			LabyrinthTile tile;
			if (r < 15) {
				tile = LabyrinthTile.OPEN;
			} else if (r < 19) {
				tile = LabyrinthTile.WALL;
			} else {
				tile = LabyrinthTile.GOLD;
			}
			grid.set(loc, tile);
		}
		int px = random.nextInt(width);
		int py = random.nextInt(height);
		
		grid.set(new Location(px, py), LabyrinthTile.PLAYER);
		
		if (!test) {											   // this if only runs if the grid is a random grid with monster and gold
		grid.set(monsterSpawn(px,py,grid),LabyrinthTile.MONSTER);  // making a monster
		}

		return grid;
	}

	/**
	 * The monster can only spawn a distance of 10 tiles or more away from the player.
	 * And the tile must not be gold or wall. 
	 * 
	 * @param playerX
	 * @param playerY
	 * @param grid
	 * @return the location at which the monster will spawn.
	 */
	public static Location monsterSpawn(int playerX, int playerY, IGrid<LabyrinthTile> grid) {
		Random rand = new Random();	
		Location newLocation = null;
		
		boolean accepted = false;
		int X = 0;
		int Y = 0;
		
		boolean free = false;
		while (!free) {
			accepted = false;
			while (!accepted) {
				int x = rand.nextInt(grid.numRows());
				int x1 = posDiff(playerX, x);
				int y = rand.nextInt(grid.numColumns());
				int y1 = posDiff(playerY, y);
				
				if (x1+y1 > 10) {
					X = x;
					Y = y;
					accepted = true;
				}
			}
			newLocation = new Location(X,Y);
			if (grid.get(newLocation) != LabyrinthTile.GOLD 
					|| grid.get(newLocation) != LabyrinthTile.WALL) {
				free = true;
			}
		}
		return newLocation;
	}
	
	/**
	 * 
	 * @param playerX
	 * @param newX
	 * @return a positive distance between player and new location in one dimension.
	 */
	public static int posDiff(int playerX, int newX) {
		int differance = 0;
		if (playerX > newX) {
			while (playerX != newX) {
				newX++;
				differance++;
			}
		} else if (playerX < newX) {
			while (playerX != newX) {
				playerX++;
				differance++;
			}
		}
		return differance;
	}
}
